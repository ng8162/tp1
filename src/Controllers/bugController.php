<?php
namespace BugApp\Controllers;

use BugApp\Models\BugManager;

use GuzzleHttp\Client;


class BugController
{

    public function add(){        
        $bugManager = new BugManager();
        $content = $this->render('src/Views/add', []);
        

        if (isset($_POST['description']) && isset($_POST['auteur']) && isset($_POST['domainName'])) {
            if (isset($_POST['etat']) && $_POST['etat']==='on') {
                $etat=1;
            }else{
                $etat=0;
            }
                $domainName = $this->getDomainName($_POST['domainName']);
                
                $responseAPI = $this->RequestApi($domainName);

                if ($responseAPI->status==='success') {
                    $ip = $responseAPI->query;
                    $bugManager->add(htmlspecialchars($_POST['description']), htmlspecialchars($_POST['auteur']),$etat,htmlspecialchars($_POST['domainName']),$ip);
                    header('Location: ../bug/list');
                    die();
                }else{
                    echo 'Le nom de dommaine saisi est inconnu';
                    die();
                }

                
                
            }
    
         

        return $this->sendHttpResponse($content, 200);
    }

    public function list(){
        /*echo'<pre>';
        var_dump(apache_request_headers());
        echo '</pre>';*/

        $bugManager = new BugManager();
        $headers = apache_request_headers();

            if (isset($headers['XMLHttpRequest'])){
                if(isset($_POST['filter'])){
                    if ($_POST['filter']==='0') {
                        $bugs = $bugManager->findByStatut(0);
                    }else if ($_POST['filter']==='all') {
                        $bugs = $bugManager->findAll();
                    }
                }else{
                    $bugs = $bugManager->findAll();
                }
                $response =[
                    'succes' => true,
                    'bugs' => $bugs,
                ];
                echo json_encode($response);
            }else{
                $bugs = $bugManager->findAll();
                $content = $this->render('src/Views/list', ['bugs' => $bugs]);
                return $this->sendHttpResponse($content, 200);
            }     
        
        }


    public function show($id){

        $bugManager = new BugManager();        
        $bug = $bugManager->find($id);
        $content = $this->render('src/Views/show', ['bug' => $bug]);
        
        return $this->sendHttpResponse($content, 200);
    }

    public function edit($id){
        $bugManager = new BugManager();        
        $bug = $bugManager->find($id);
        $content = $this->render('src/Views/edit', ['bug' => $bug]);
        
        return $this->sendHttpResponse($content, 200);
    }

    public function update($id){
        $bugManager = new BugManager();
        $bug = $bugManager->find($id);
        //if (isset($_POST)) {
            if (isset($_POST['etat'])) {

                $bug->setEtat($_POST['etat']);
            }
        
            $bugManager->update($bug);

            http_response_code(200);
            header('Content-type: application/json');
            $response =[
                'succes' => true,
                'id' => $bug->getId(),
            ];
            echo json_encode($response);
        //}
        

        
    }

    public function render($templatePath, $params){
        $templatePath = $templatePath . ".php";
        $params;
        
        require($templatePath);

        return ob_get_clean();
    }

    public static function sendHttpResponse($content, $code = 200){
        http_response_code($code);

        header('Content-type: text/html');

        echo $content;
    }

    public function RequestApi($domainName){
        $client = new Client();
        $response = $client->request('GET', "http://ip-api.com/json/$domainName");

        //echo $response->getStatusCode(); // 200
        //echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        //echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'

        $JsonResponse = $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'
        //print_r($responseAPI);
        return $responseAPI = json_decode($JsonResponse);
    }
   
    public function getDomainName($url){
       $resultParseUrl = parse_url($url);
       //var_dump($resultParseUrl);
       $domainName = $resultParseUrl['host'];

       //var_dump($domainName);

       return $domainName;
    }

}
