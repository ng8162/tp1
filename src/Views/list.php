﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../src/Ressources/css/style.css">
    <title>Liste des Bugs</title>
</head>
<body>
    
<h1><span class="blue">&lt;</span>Bugs<span class="blue">&gt;</span> <span class="yellow">List</pan></h1>
<h2>Created by <a href="#" target="_blank">Nicolas glories</a></h2>
<label for="filter">afficher les bugs non resolus</label>
<input class="filter" type="checkbox" name="filter" id="filter">
<button class="boutonAjouter" title="Ajouter un Bug"><a style="font-size: 50px; color: inherit; text-decoration: none;" href="../bug/add">+</a></button>
<table class="container">
    <thead> <!-- En-tête du tableau -->
       <tr>
           <th><h1>id</h1></th>
           <th><h1>description du bug</h1></th>
           <th><h1>Auteur</h1></th>
           <th><h1>date</h1></th>
           <th><h1>etat</h1></th>
           <th><h1>Nom de domaine</h1></th>
           <th><h1>Adresse IP</h1></th>
           <th><h1>details</h1></th>
           <th><h1>Edition</h1></th>
   </thead>

   <tfoot> <!-- Pied de tableau -->
        <tr>
           <th><h1>id</h1></th>
           <th><h1>description du bug</h1></th>
           <th><h1>Auteur</h1></th>
           <th><h1>date</h1></th>
           <th><h1>etat</h1></th>
           <th><h1>Nom de domaine</h1></th>
           <th><h1>Adresse IP</h1></th>
           <th><h1>details</h1></th>
           <th><h1>Edition</h1></th>
       </tr>
   </tfoot>

   <tbody id="tbody">        
        <?php
        /*echo '<pre>';
        var_dump($params);
        echo '</pre>';*/

        $bugs = $params['bugs'];
        
        /*echo '<pre>';
        var_dump($bugs);
        echo '</pre>';*/


        foreach ($bugs as $bug) {
            if ($bug->getEtat() == '0') {
                $etat = '<td id=td_'.$bug->getId().'><a class="stateRed trigger" href="">Non traiter</a></td>';
            }elseif ($bug->getEtat() == '1') {
                $etat = '<td id=td_'.$bug->getId().'><span class=stateGreen >Cloturer</span></td>';
            }?>
        <tr class=row id=bug_<?php echo $bug->getId() ?>>
            <td><?php echo $bug->getId(); ?></td>
            <td><?php echo $bug->getDescription(); ?></td>
            <td><?php echo $bug->getAuteur(); ?></td>
            <td><?php echo $bug->getCreatedAt(); ?></td>
            <?php echo $etat;?>
            <td><?php echo $bug->getDomainName(); ?></td>
            <td><?php echo $bug->getIp(); ?></td>
            <td><a href=../bug/show/<?php echo $bug->getId(); ?>>Details</a></td>
            <td><a href=../bug/edit/<?php echo $bug->getId(); ?>>Editer</a></td> 
        </tr>
    <?php } ?>
   </tbody>
</table>

<script src="../src/Ressources/script/ajax.js"></script>
</body>
</html>