<?php
require 'vendor/autoload.php';
use BugApp\Controllers\BugController;
$bugController = new BugController();
$url = explode('/',$_SERVER["REQUEST_URI"]);

if ($url[4] == 'bug' && $url[5] == 'list') {
    return $bugController->list();
}else if ($url[3] == 'tp1' && $url[4] === "" && !isset($url[5])) {
    header('location: '.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'bug/list');
}else if ($url[4] == 'bug' && $url[5] == 'add') {
   return $bugController->add();
}else if ($url[4] == 'bug' && $url[5] == 'show') {
    return $bugController->show($url[6]);
}else if ($url[4] == 'bug' && $url[5] == 'update') {
    return $bugController->update($url[6]);
}else if ($url[4] == 'bug' && $url[5] == 'edit') {
    return $bugController->edit($url[6]);
} else {
    require '404.php';
}
