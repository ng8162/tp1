-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 23 oct. 2019 à 22:40
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bug`
--

-- --------------------------------------------------------

--
-- Structure de la table `buglist`
--

CREATE TABLE `buglist` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `auteur` varchar(100) NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `buglist`
--

INSERT INTO `buglist` (`id`, `description`, `auteur`, `etat`, `created_at`) VALUES
(1, 'description1', 'n.g.@domaine1.fr', 1, '2019-09-28 00:00:00'),
(2, 'description2', 'n.g.@domaine2.fr', 1, '2019-09-28 00:00:00'),
(3, 'description3', 'n.g.@domaine3.fr', 0, '2019-09-28 00:00:00'),
(4, 'description4', 'n.g@domaine2.fr', 1, '2019-09-28 00:00:00'),
(5, 'test', 'test@mp.fr', 0, '2019-09-28 00:00:00'),
(6, 'test1', 'test@mp.fr', 0, '2019-09-28 00:00:00'),
(8, 'test22', 'test@mp2.fr', 1, '2019-09-28 00:00:00'),
(9, 'test', 'test@mp.fr', 0, '2019-09-28 00:00:00'),
(10, 'test', 'test@mp.fr', 0, '2019-09-28 00:00:00'),
(11, 'test444444444444444444', 'test@mp444.fr', 1, '2019-09-28 00:00:00'),
(12, 'test55555555555555555555', 'test@mp555.fr', 0, '2019-09-28 00:00:00'),
(13, 'test', 'test@mp.fr', 0, '2019-10-18 16:43:56'),
(14, 'test add routage', 'test@test.fr', 1, '2019-10-22 18:04:13'),
(15, 'test add routage', 'test@test.fr', 0, '2019-10-22 18:04:49'),
(16, 'test add route', 'test@test.fr', 0, '2019-10-22 18:05:25'),
(17, 'template add', 'test@test.fr', 0, '2019-10-23 17:50:31');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `buglist`
--
ALTER TABLE `buglist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `buglist`
--
ALTER TABLE `buglist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
